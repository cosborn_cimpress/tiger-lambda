// <copyright file="HandlerGenerator.cs" company="Cimpress, Inc.">
//   Copyright 2021 Cimpress, Inc.
//
//   Licensed under the Apache License, Version 2.0 (the "License") –
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
// </copyright>

using System;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Text;
using static Microsoft.CodeAnalysis.LanguageNames;

namespace Tiger.Lambda;

/// <summary>Generates the dependency-injection handler for Lambda Functions.</summary>
[Generator(CSharp)]
public sealed class HandlerGenerator
    : IIncrementalGenerator
{
    static readonly Encoding s_utf8 = new UTF8Encoding(encoderShouldEmitUTF8Identifier: false);

    /// <inheritdoc/>
    void IIncrementalGenerator.Initialize(IncrementalGeneratorInitializationContext context)
    {
        var results = context
            .SyntaxProvider
            .CreateSyntaxProvider(
                static (s, _) => Parser.IsSyntaxTargetForGeneration(s),
                static (ctx, ct) => Parser.GetSemanticTargetForGeneration(ctx, ct))
            .Where(static cds => cds is not null)
            .Select(static (cds, _) => cds!); // todo(cosborn) Why does flow analysis fail here, but not for MS?
        var compilationAndResults = context.CompilationProvider.Combine(results.Collect());
        context.RegisterSourceOutput(compilationAndResults, static (spc, source) => Execute(source.Left, source.Right, spc));
    }

    static void Execute(Compilation compilation, ImmutableArray<ClassDeclarationSyntax> classDecls, SourceProductionContext context)
    {
        if (classDecls.IsDefaultOrEmpty)
        {
            return;
        }

        var p = new Parser(compilation, context.ReportDiagnostic, context.CancellationToken);
        var entryData = p.GetEntryData(classDecls);
        if (entryData.Count > 0)
        {
            var result = Emitter.Emit(entryData, context.CancellationToken);

            context.AddSource("Tiger.Lambda.g.cs", SourceText.From(result, s_utf8));
        }
    }
}
