// <copyright file="Parser.cs" company="Cimpress, Inc.">
//   Copyright 2021 Cimpress, Inc.
//
//   Licensed under the Apache License, Version 2.0 (the "License") –
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
// </copyright>

using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using static Microsoft.CodeAnalysis.CSharp.SyntaxKind;
using static Microsoft.CodeAnalysis.SymbolDisplayFormat;

namespace Tiger.Lambda;

/// <summary>Parses source code looking for handler source generator hooks.</summary>
sealed class Parser
{
    const string CancellationTokenType = "System.Threading.CancellationToken";
    const string FunctionType = "Tiger.Lambda.Function";
    const string LambdaContextType = "Amazon.Lambda.Core.ILambdaContext";
    const string LambdaHandler1AttributeType = "Tiger.Lambda.LambdaHandlerAttribute<>";
    const string LambdaHandler2AttributeType = "Tiger.Lambda.LambdaHandlerAttribute<,>";
    const string TaskType = "System.Threading.Tasks.Task";
    const string Task1Type = "System.Threading.Tasks.Task<>";

    readonly Compilation _compilation;
    readonly Action<Diagnostic> _reportDiagnostic;
    readonly CancellationToken _cancellationToken;

    /// <summary>Initializes a new instance of the <see cref="Parser"/> class.</summary>
    /// <param name="compilation">The current compilation.</param>
    /// <param name="reportDiagnostic">An action which will report a diagnostic to the current project.</param>
    /// <param name="cancellationToken">A token to watch for operation cancellation.</param>
    public Parser(Compilation compilation, Action<Diagnostic> reportDiagnostic, CancellationToken cancellationToken)
    {
        _compilation = compilation;
        _cancellationToken = cancellationToken;
        _reportDiagnostic = reportDiagnostic;
    }

    /// <summary>Decides whether a syntax node is a target for handler generation.</summary>
    /// <param name="node">The syntax node to check.</param>
    /// <returns>
    /// <see langword="true"/> if <paramref name="node"/> is such a syntax target;
    /// otherwise, <see langword="false"/>.
    /// </returns>
    public static bool IsSyntaxTargetForGeneration(SyntaxNode node) =>
        node is ClassDeclarationSyntax cds && cds.AttributeLists.Count > 0;

#pragma warning disable
    /// <summary>Gets the parsing result to be used as a target of code generation.</summary>
    /// <param name="context">The context of the current generator.</param>
    /// <param name="cancellationToken">A token to watch for operation cancellation.</param>
    /// <returns>The paring result, or <see langword="null"/>.</returns>
    public static ClassDeclarationSyntax? GetSemanticTargetForGeneration(GeneratorSyntaxContext context, CancellationToken cancellationToken)
    {
        var classDecl = (ClassDeclarationSyntax)context.Node;
        foreach (var attr in classDecl.AttributeLists.SelectMany(a => a.Attributes))
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (context.SemanticModel.GetSymbolInfo(attr, cancellationToken).Symbol is not { ContainingType: { IsGenericType: true, Arity: 2 } ct })
            {
                continue;
            }

            var fullName = ct.ConstructUnboundGenericType().ToDisplayString();
            if (fullName == LambdaHandler1AttributeType || fullName == LambdaHandler2AttributeType)
            {
                return classDecl;
            }
        }

        return null;
    }

    /// <summary>Transforms generation target results into the data required to generate source code.</summary>
    /// <param name="classDecls">The generation target results.</param>
    /// <returns>A collection of handler data.</returns>
    public IReadOnlyCollection<GeneratorState> GetEntryData(IReadOnlyCollection<ClassDeclarationSyntax> classDecls)
    {
        var lambdaContext = _compilation.GetTypeByMetadataName(LambdaContextType);
        if (lambdaContext is null)
        {
            // note(cosborn) Nothing to do if this type isn't available.
            return Array.Empty<GeneratorState>();
        }

        var cancellationToken = _compilation.GetTypeByMetadataName(CancellationTokenType);
        if (cancellationToken is null)
        {
            // note(cosborn) Nothing to do if this type isn't available.
            return Array.Empty<GeneratorState>();
        }

        var serviceProvider = _compilation.GetTypeByMetadataName("System.IServiceProvider");
        if (serviceProvider is null)
        {
            // note(cosborn) Nothing to do if this type isn't available.
            return Array.Empty<GeneratorState>();
        }

        var entryData = new List<GeneratorState>(classDecls.Count);
        foreach (var group in classDecls.GroupBy(c => c.SyntaxTree))
        {
            var semanticModel = _compilation.GetSemanticModel(group.Key);
            foreach (var classDecl in group)
            {
                _cancellationToken.ThrowIfCancellationRequested();

                if (semanticModel.GetDeclaredSymbol(classDecl) is not { } entrySym)
                {
                    // note(cosborn) I don't even know what to report here. It's literally a class definition.
                    continue;
                }

                if (!classDecl.Modifiers.Any(m => m.IsKind(PartialKeyword)))
                {
                    _reportDiagnostic(Diagnostic.Create(DiagnosticDescriptors.EntryIsNotPartial, classDecl.Identifier.GetLocation(), classDecl.Keyword, entrySym.ToDisplayString(CSharpShortErrorMessageFormat)));
                    continue;
                }

                if (entrySym.IsAbstract)
                {
                    _reportDiagnostic(Diagnostic.Create(DiagnosticDescriptors.HandlerIsAbstract, classDecl.Identifier.GetLocation(), classDecl.Keyword, entrySym.ToDisplayString(CSharpShortErrorMessageFormat)));
                    continue;
                }

                if (entrySym.IsGenericType)
                {
                    _reportDiagnostic(Diagnostic.Create(DiagnosticDescriptors.EntryIsGeneric, classDecl.Identifier.GetLocation(), classDecl.Keyword, entrySym.ToDisplayString(CSharpShortErrorMessageFormat)));
                    continue;
                }

                if (entrySym.IsStatic || !DescendsFrom(entrySym, FunctionType))
                {
                    _reportDiagnostic(Diagnostic.Create(DiagnosticDescriptors.EntryIsNotFunction, classDecl.Identifier.GetLocation(), classDecl.Keyword, entrySym.ToDisplayString(CSharpShortErrorMessageFormat), FunctionType));
                }

                foreach (var attr in classDecl.AttributeLists.SelectMany(al => al.Attributes))
                {
                    if (semanticModel.GetSymbolInfo(attr, _cancellationToken).Symbol is not IMethodSymbol { ContainingType: { IsGenericType: true } ct } ctor)
                    {
                        continue;
                    }

                    var unboundCt = ct.ConstructUnboundGenericType().ToDisplayString();
                    if (unboundCt != LambdaHandler1AttributeType && unboundCt != LambdaHandler2AttributeType)
                    {
                        continue;
                    }

                    var handlerSym = ct.TypeArguments[0];
                    var serializerSym = ct.TypeArguments.Length == 2 ? ct.TypeArguments[1] : null;

                    var handlerDatum = GetHandlerDatum(semanticModel, attr, handlerSym, serializerSym);
                    if (handlerDatum is null)
                    {
                        // note(cosborn) GetHandlerDatum emits the relevant diagnostics.
                        continue;
                    }

                    var generatorState = new GeneratorState(serializerSym)
                    {
                        Entry = new GeneratorState.EntryDatum
                        {
                            Keyword = classDecl.Keyword.ValueText,
                            Namespace = GetNamespace(classDecl),
                            TypeName = classDecl.Identifier.ToString() + classDecl.TypeParameterList,
                            Constraints = classDecl.ConstraintClauses.ToString(),
                            Parent = null,
                        },
                        Handler = handlerDatum,
                    };

                    var currentEntry = generatorState.Entry;
                    var potentialParent = classDecl.Parent as TypeDeclarationSyntax;
                    while (potentialParent != null && IsAllowedKind(potentialParent.Kind()))
                    {
                        currentEntry.Parent = currentEntry with
                        {
                            Keyword = potentialParent.Keyword.ValueText,
                            TypeName = potentialParent.Identifier.ToString() + potentialParent.TypeParameterList,
                            Constraints = potentialParent.ConstraintClauses.ToString(),
                            Parent = null,
                        };

                        currentEntry = currentEntry.Parent;
                        potentialParent = potentialParent.Parent as TypeDeclarationSyntax;
                    }

                    entryData.Add(generatorState);

                    static bool IsAllowedKind(SyntaxKind kind) =>
                        kind == ClassDeclaration ||
                        kind == StructDeclaration ||
                        kind == RecordDeclaration;
                }
            }
        }

        return entryData;

        static string? GetNamespace(ClassDeclarationSyntax entryDecl)
        {
            string? @namespace = null;

            var potentialNamespaceParent = entryDecl.Parent;
            while (potentialNamespaceParent is not null
                && potentialNamespaceParent is not NamespaceDeclarationSyntax
                && potentialNamespaceParent is not FileScopedNamespaceDeclarationSyntax)
            {
                potentialNamespaceParent = potentialNamespaceParent.Parent;
            }

            var namespaceParent = potentialNamespaceParent as BaseNamespaceDeclarationSyntax;
            if (namespaceParent is not null)
            {
                @namespace = namespaceParent.Name.ToString();
                while (true)
                {
                    namespaceParent = namespaceParent.Parent as NamespaceDeclarationSyntax;
                    if (namespaceParent is null)
                    {
                        break;
                    }

                    @namespace = $"{namespaceParent.Name}.{@namespace}";
                }
            }

            return @namespace;
        }

        static bool DescendsFrom(ITypeSymbol type, string descendant)
        {
            var candidateType = type;
            while (candidateType is not null)
            {
                if (candidateType.ToDisplayString() == descendant)
                {
                    return true;
                }

                candidateType = candidateType.BaseType;
            }

            return false;
        }

        GeneratorState.HandlerDatum? GetHandlerDatum(
            SemanticModel semanticModel,
            AttributeSyntax attr,
            ITypeSymbol handlerSym,
            ITypeSymbol? serializerSym)
        {
            var voidType = _compilation.GetSpecialType(SpecialType.System_Void);
            var candidates = handlerSym
                .GetMembers("HandleAsync")
                .Concat(handlerSym.GetMembers("Handle"))
                .OfType<IMethodSymbol>()
                .ToImmutableArray();

            if (candidates.Length > 1)
            {
                _reportDiagnostic(Diagnostic.Create(
                    DiagnosticDescriptors.EntryHasTooManyHandlers,
                    attr.GetLocation(),
                    handlerSym.ToDisplayString(CSharpErrorMessageFormat)));
                return null;
            }

            if (candidates.Length == 0)
            {
                _reportDiagnostic(Diagnostic.Create(
                    DiagnosticDescriptors.EntryHasNoHandler,
                    attr.GetLocation(),
                    handlerSym.ToDisplayString(CSharpShortErrorMessageFormat)));
                return null;
            }

            var candidate = candidates[0];

            if (candidate.Parameters.Length == 0)
            {
                _reportDiagnostic(Diagnostic.Create(
                    DiagnosticDescriptors.HandlerHasTooFewParameters,
                    candidate.Locations[0],
                    candidate.Locations.Skip(1),
                    candidate.ToDisplayString(CSharpShortErrorMessageFormat)));
                return null;
            }

            if (candidate.IsStatic)
            {
                _reportDiagnostic(Diagnostic.Create(
                    DiagnosticDescriptors.HandlerIsStatic,
                    candidate.Locations[0],
                    candidate.Locations.Skip(1),
                    candidate.ToDisplayString(CSharpShortErrorMessageFormat)));
                return null;
            }

            if (candidate.IsAbstract)
            {
                _reportDiagnostic(Diagnostic.Create(
                    DiagnosticDescriptors.HandlerIsAbstract,
                    candidate.Locations[0],
                    candidate.Locations.Skip(1),
                    candidate.ToDisplayString(CSharpShortErrorMessageFormat)));
                return null;
            }

            if (candidate.IsGenericMethod)
            {
                _reportDiagnostic(Diagnostic.Create(
                    DiagnosticDescriptors.HandlerIsGenericMethod,
                    candidate.Locations[0],
                    candidate.Locations.Skip(1),
                    candidate.ToDisplayString(CSharpShortErrorMessageFormat)));
                return null;
            }

            // note(cosborn) Error especially about async void.
            var isVoid = SymbolEqualityComparer.Default.Equals(candidate.ReturnType, voidType);
            if (candidate.IsAsync && isVoid)
            {
                _reportDiagnostic(Diagnostic.Create(
                    DiagnosticDescriptors.HandlerIsAsyncVoid,
                    candidate.Locations[0],
                    candidate.Locations.Skip(1),
                    candidate.ToDisplayString(CSharpShortErrorMessageFormat)));
                return null;
            }

            if (candidate.ReturnType is not INamedTypeSymbol returnType)
            {
                return null;
            }

            var isTask = returnType.ToDisplayString() == TaskType;
            var isAsync = isTask || (returnType.IsGenericType && returnType.Arity == 1 && returnType.ConstructUnboundGenericType().ToDisplayString() == Task1Type);

            var hasLambdaContext = candidate.Parameters.Length >= 2 && candidate.Parameters[1].ToDisplayString() == LambdaContextType;
            var hasCancellationToken = candidate.Parameters.Length >= 2 && candidate.Parameters[candidate.Parameters.Length - 1].ToDisplayString() == CancellationTokenType;

            /* note(cosborn)
             * Given the requirement of an input value, the resolvable parameters will be
             * 1 or 2 from the start of the parameter list and 0 or 1 from the end.
             */
            var (resolvableStart, resolvableLength) = (
                HasLambdaContext: hasLambdaContext,
                HasCancellationToken: hasCancellationToken,
                candidate.Parameters.Length) switch
            {
                (false, false, var l) => (Start: 1, Length: l - 1),
                (false, true, var l) => (Start: 1, Length: l - 2),
                (true, false, var l) => (Start: 2, Length: l - 2),
                (true, true, var l) => (Start: 2, Length: l - 3),
            };
            var resolvable = ImmutableArray.Create(candidate.Parameters, resolvableStart, resolvableLength);

            var handlerDatum = new GeneratorState.HandlerDatum
            {
                Name = candidate.Name,
                Type = handlerSym,
                Input = candidate.Parameters[0].Type,
                Output = returnType,
                Resolvable = resolvable,
                HasLambdaContext = hasLambdaContext,
                HasCancellationToken = hasCancellationToken,
                IsAsync = isAsync,
                IsEffectivelyVoid = isVoid || isTask,
            };

            if ((handlerDatum.MustDeserializeInput || handlerDatum.MustSerializeOutput) && serializerSym is null)
            {
                _reportDiagnostic(Diagnostic.Create(
                    DiagnosticDescriptors.HandlerRequiresSerializer,
                    candidate.Locations[0],
                    candidate.Locations.Skip(1),
                    candidate.ToDisplayString(CSharpShortErrorMessageFormat)));
            }

            return handlerDatum;
        }
    }
}
