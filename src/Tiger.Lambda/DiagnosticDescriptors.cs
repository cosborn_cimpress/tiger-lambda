// <copyright file="DiagnosticDescriptors.cs" company="Cimpress, Inc.">
//   Copyright 2021 Cimpress, Inc.
//
//   Licensed under the Apache License, Version 2.0 (the "License") –
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
// </copyright>

using Microsoft.CodeAnalysis;
using static Microsoft.CodeAnalysis.DiagnosticSeverity;

namespace Tiger.Lambda;

/// <summary>The descriptors for emitted diagnostics.</summary>
public static class DiagnosticDescriptors
{
    /// <summary>Gets the diagnostic for when an entry type is not partial.</summary>
    public static DiagnosticDescriptor EntryIsNotPartial { get; } = new DiagnosticDescriptor(
        id: "TL1001",
        title: "Entry type is not partial.",
        messageFormat: "The {0} {1} is not marked partial.",
        category: "Tiger.Lambda",
        defaultSeverity: Error,
        isEnabledByDefault: true,
        description: "The entry type must be marked as partial for the code generation to generate an entry point for the invocation.");

    /// <summary>Gets the diagnostic for when an entry type is abstract.</summary>
    public static DiagnosticDescriptor EntryIsAbstract { get; } = new DiagnosticDescriptor(
        id: "TL1002",
        title: "Entry type is abstract.",
        messageFormat: "The {0} {1} is abstract.",
        category: "Tiger.Lambda",
        defaultSeverity: Error,
        isEnabledByDefault: true,
        description: "The entry type cannot be abstract – Lambda must instantiate the type to call the handler method.");

    /// <summary>Gets the diagnostic for when an entry type is generic (has type parameters).</summary>
    public static DiagnosticDescriptor EntryIsGeneric { get; } = new DiagnosticDescriptor(
        id: "TL1002",
        title: "Entry type is generic.",
        messageFormat: "The {0} {1} has type parameters.",
        category: "Tiger.Lambda",
        defaultSeverity: Error,
        isEnabledByDefault: true,
        description: "The entry type cannot be generic – Lambda has no way to supply type parameters.");

    /// <summary>Gets the diagnostic for when an entry type does not inherit from <see cref="Function"/>.</summary>
    public static DiagnosticDescriptor EntryIsNotFunction { get; } = new DiagnosticDescriptor(
        id: "TL1002",
        title: "Entry type does not inherit from required base class.",
        messageFormat: "The {0} {1} has does not inherit from {2}.",
        category: "Tiger.Lambda",
        defaultSeverity: Error,
        isEnabledByDefault: true,
        description: "The entry type must inherit from Tiger.Lambda's Function class, which contains hooks for code generation.");

    /// <summary>Gets the diagnostic for when too many handler methods could be found.</summary>
    public static DiagnosticDescriptor EntryHasTooManyHandlers { get; } = new DiagnosticDescriptor(
        id: "TL2001",
        title: "Too many handlers found.",
        messageFormat: "Multiple methods named Handle or HandleAsync were found on type {0}.",
        category: "Tiger.Lambda",
        defaultSeverity: Error,
        isEnabledByDefault: true,
        description: "The build process does not attempt to handle overloads or multiple handler candidates.");

    /// <summary>Gets the diagnostic for when no handler methods could be found.</summary>
    public static DiagnosticDescriptor EntryHasNoHandler { get; } = new DiagnosticDescriptor(
        id: "TL2002",
        title: "No handler found.",
        messageFormat: "No method named Handle or HandleAsync with the appropriate signature could be found on type {0}.",
        category: "Tiger.Lambda",
        defaultSeverity: Error,
        isEnabledByDefault: true,
        description: "The build process looks for methods with certain names, and no such methods were found.");

    /// <summary>Gets the diagnostic for when an async void handler is rejected.</summary>
    public static DiagnosticDescriptor HandlerIsAsyncVoid { get; } = new DiagnosticDescriptor(
        id: "TL2003",
        title: "Invalid handler rejected.",
        messageFormat: "The handler {0} was rejected because it claims to be a void-returning asynchronous method.",
        category: "Tiger.Lambda",
        defaultSeverity: Error,
        isEnabledByDefault: true,
        description: "Asynchronous Function handlers cannot be 'async void' methods. With no knowledge of running Tasks, Lambda will terminate execution as soon as the method returns. Asynchronous handlers must return Task or Task<T>.");

    /// <summary>Gets the diagnostic for when a handler with too few parameters is rejected.</summary>
    public static DiagnosticDescriptor HandlerHasTooFewParameters { get; } = new DiagnosticDescriptor(
        id: "TL2004",
        title: "Invalid handler rejected.",
        messageFormat: "The handler {0} was rejected because it has too few parameters.",
        category: "Tiger.Lambda",
        defaultSeverity: Error,
        isEnabledByDefault: true,
        description: "Function handlers must, at a minimum, accept an input value.");

    /// <summary>Gets the diagnostic for when a handler is static.</summary>
    public static DiagnosticDescriptor HandlerIsStatic { get; } = new DiagnosticDescriptor(
        id: "TL2005",
        title: "Invalid handler rejected.",
        messageFormat: "The handler {0} was rejected because it is static.",
        category: "Tiger.Lambda",
        defaultSeverity: Error,
        isEnabledByDefault: true,
        description: "Tiger.Lambda does not currently support static handlers. Please change the handler to an instance method.");

    /// <summary>Gets the diagnostic for when a handler is abstract.</summary>
    public static DiagnosticDescriptor HandlerIsAbstract { get; } = new DiagnosticDescriptor(
        id: "TL2006",
        title: "Invalid handler rejected.",
        messageFormat: "The handler {0} was rejected because it is abstract.",
        category: "Tiger.Lambda",
        defaultSeverity: Error,
        isEnabledByDefault: true,
        description: "An abstract handler method cannot be invoked.");

    /// <summary>Gets the diagnostic for when a handler is a generic method.</summary>
    public static DiagnosticDescriptor HandlerIsGenericMethod { get; } = new DiagnosticDescriptor(
        id: "TL2006",
        title: "Invalid handler rejected.",
        messageFormat: "The handler {0} was rejected because it is a generic method.",
        category: "Tiger.Lambda",
        defaultSeverity: Error,
        isEnabledByDefault: true,
        description: "A handler method with type parameters cannot be invoked.");

    /// <summary>Gets the diagnostic for when a handler requires a serializer, but doesn't have one.</summary>
    public static DiagnosticDescriptor HandlerRequiresSerializer { get; } = new DiagnosticDescriptor(
        id: "TL2007",
        title: "Handler required further configuration.",
        messageFormat: "The handler {0} requires a serializer (or deserializer), but none was provided.",
        category: "Tiger.Lambda",
        defaultSeverity: Error,
        isEnabledByDefault: true,
        description: "AWS Lambda can only accept and return streams without serialization of deserialization. If the input to or output from handler method is anything other than System.IO.Stream, a serializer must be provided.");
}
