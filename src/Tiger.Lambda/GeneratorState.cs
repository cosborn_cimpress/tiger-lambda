// <copyright file="GeneratorState.cs" company="Cimpress, Inc.">
//   Copyright 2021 Cimpress, Inc.
//
//   Licensed under the Apache License, Version 2.0 (the "License") –
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
// </copyright>

using System.Collections.Immutable;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Lambda.Core;
using Microsoft.CodeAnalysis;
using static System.StringComparer;
using static Microsoft.CodeAnalysis.SymbolDisplayFormat;

namespace Tiger.Lambda;

/// <summary>The state values for a generation.</summary>
sealed record class GeneratorState
{
    readonly ITypeSymbol? _serializer;

    /// <summary>Initializes a new instance of the <see cref="GeneratorState"/> class.</summary>
    /// <param name="serializer">The optional serializer.</param>
    public GeneratorState(ITypeSymbol? serializer = null)
    {
        _serializer = serializer;
    }

    /// <summary>Gets the data representing the entry type.</summary>
    public EntryDatum Entry { get; init; } = new EntryDatum();

    /// <summary>Gets the data representing the handler type.</summary>
    public HandlerDatum Handler { get; init; } = new HandlerDatum();

    /// <summary>Gets the serializer type, if present.</summary>
    public ITypeSymbol? Serializer => !Handler.MustDeserializeInput && !Handler.MustSerializeOutput ? null : _serializer;

    public sealed record class EntryDatum
    {
        /// <summary>Gets the namespace of the entry type, if any.</summary>
        public string? Namespace { get; init; }

        /// <summary>Gets the keyword use to define an entry type.</summary>
        public string Keyword { get; init; } = string.Empty;

        /// <summary>Gets the entry type name, with parameters.</summary>
        public string TypeName { get; init; } = null!;

        /// <summary>Gets the constraints applied to the entry type.</summary>
        public string Constraints { get; init; } = string.Empty;

        /// <summary>Gets or sets the data representing a parent to the entry type, if any.</summary>
        public EntryDatum? Parent { get; set; }
    }

    public sealed record class HandlerDatum
    {
        /// <summary>Gets the name of the handler.</summary>
        public string Name { get; init; } = "Handle";

        /// <summary>Gets the symbol representing the type which ultimately handles the Function.</summary>
        public ITypeSymbol Type { get; init; } = null!;

        /// <summary>Gets the symbol representing input type.</summary>
        public ITypeSymbol Input { get; init; } = null!;

        /// <summary>Gets the symbol representing output type.</summary>
        public INamedTypeSymbol Output { get; init; } = null!;

        /// <summary>Gets the symbols representing the resolvable parameters of a handler.</summary>
        public ImmutableArray<IParameterSymbol> Resolvable { get; init; } = ImmutableArray<IParameterSymbol>.Empty;

        /// <summary>Gets a value indicating whether the handler method accepts a <see cref="ILambdaContext"/>.</summary>
        public bool HasLambdaContext { get; init; }

        /// <summary>Gets a value indicating whether the handler method accepts a <see cref="CancellationToken"/>.</summary>
        public bool HasCancellationToken { get; init; }

        /// <summary>Gets a value indicating whether the handler is async.</summary>
        /// <remarks>Lambda only handles <see cref="Task"/> and <see cref="Task{T}"/>.</remarks>
        public bool IsAsync { get; init; }

        /// <summary>Gets a value indicating whether the handler is effectively void.</summary>
        /// <remarks>This means <see cref="void"/> or <see cref="Task"/>.</remarks>
        public bool IsEffectivelyVoid { get; init; }

        /// <summary>Gets a value indicating whether the handler's input must be deserialized.</summary>
        public bool MustDeserializeInput => !Ordinal.Equals(Input?.ToDisplayString(FullyQualifiedFormat), "global::System.IO.Stream");

        /// <summary>Gets a value indicating whether the handler's output must be serialized.</summary>
        public bool MustSerializeOutput => !Ordinal.Equals(Output?.ToDisplayString(FullyQualifiedFormat), "global::System.IO.Stream");
    }
}
