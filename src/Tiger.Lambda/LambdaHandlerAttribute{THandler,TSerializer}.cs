// <copyright file="LambdaHandlerAttribute{THandler,TSerializer}.cs" company="Cimpress, Inc.">
//   Copyright 2021 Cimpress, Inc.
//
//   Licensed under the Apache License, Version 2.0 (the "License") –
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
// </copyright>

using System;
using Amazon.Lambda.Core;
using static System.AttributeTargets;

namespace Tiger.Lambda;

/// <summary>Identifies the handler and serializer for this entry type.</summary>
/// <typeparam name="THandler">The handler type for the attached entry type.</typeparam>
/// <typeparam name="TSerializer">The serializdr type for the attached entry type.</typeparam>
[AttributeUsage(Class, Inherited = false)]
public sealed class LambdaHandlerAttribute<THandler, TSerializer>
    : Attribute
    where TSerializer : ILambdaSerializer, new()
{
}
