// <copyright file="Trial.cs" company="Cimpress, Inc.">
//   Copyright 2021 Cimpress, Inc.
//
//   Licensed under the Apache License, Version 2.0 (the "License") –
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
// </copyright>

using System;
using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Lambda.Core;
using Amazon.Lambda.Serialization.SystemTextJson;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Tiger.Lambda;
using static System.Globalization.CultureInfo;
using static System.Text.Json.Serialization.JsonKnownNamingPolicy;
using static Microsoft.Extensions.Logging.LogLevel;

namespace Testation;

/// <summary>You have been found guilty. Go to jail.</summary>
[SuppressMessage("StyleCop", "SA1015", Justification = "Generic attributes are new.")]
[LambdaHandler<Handler, SourceGeneratorLambdaJsonSerializer<JsonSerializerContext>>]
public sealed partial class Trial
    : Function
{
    /// <inheritdoc/>
    public override IHostBuilder ConfigureHostBuilder(IHostBuilder hostBuilder) => hostBuilder
        .UseSerilog((context, log) => log.ReadFrom.Configuration(context.Configuration));

    /// <summary>Wow.</summary>
    public sealed partial class Handler
    {
        readonly ILogger<Handler> _logger;

        /// <summary>Initializes a new instance of the <see cref="Handler"/> class.</summary>
        /// <param name="logger">An application logger, specialized for <see cref="Handler"/>.</param>
        public Handler(ILogger<Handler> logger)
        {
            _logger = logger;
        }

        /// <summary>Handles.</summary>
        /// <param name="input">The input.</param>
        /// <param name="context">The context of this invocation.</param>
        /// <param name="configuration">The application's configuration, resolved for demonstration purposes.</param>
        /// <param name="cancellationToken">A token to watch for operation cancellation.</param>
        /// <returns>A task which, when resolved, results in the output from the Function.</returns>
        public async Task<int> HandleAsync(
            string input,
            ILambdaContext context,
            IConfiguration configuration,
            CancellationToken cancellationToken)
        {
            ArgumentNullException.ThrowIfNull(context);

            TimeToDuel(context.AwsRequestId);
            cancellationToken.ThrowIfCancellationRequested();
            _ = configuration;
            await Task.CompletedTask.ConfigureAwait(false);
            return int.Parse(input?.ToString() ?? string.Empty, InvariantCulture);
        }

        [LoggerMessage(Level = Information, Message = "It's time to duel! {AwsRequestId}")]
        partial void TimeToDuel(string awsRequestId);
    }

    [JsonSerializable(typeof(string))]
    [JsonSerializable(typeof(int))]
    [JsonSourceGenerationOptions(
        DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingDefault,
        PropertyNamingPolicy = CamelCase)]
    sealed partial class JsonSerializerContext
        : System.Text.Json.Serialization.JsonSerializerContext
    {
    }
}
